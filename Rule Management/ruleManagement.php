<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Reward System</title>

		<!-- Bootstrap -->
		<link href="css/fontface.css" rel="stylesheet">
	    <link href="css/bootstrap.css" rel="stylesheet">
	    <link href="css/style.css" rel="stylesheet">
		<script src="jquery-1.11.0.min.js"></script>
		<script>
			function showResult(){
				var xmlhttp=new XMLHttpRequest();
				var getRules = true;	
				xmlhttp.onreadystatechange=function()
				{
					if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						document.getElementById("result").innerHTML=xmlhttp.responseText;
					}
				}
				xmlhttp.open("GET","rule_client.php?getRules="+getRules,true);
					
				xmlhttp.send();
				
			}
				
			function addRule(){
				//$("#addForm").hide("slow");		
				var xmlhttp=new XMLHttpRequest();
				var addRule = true;	
				var getRules = true;				
				var rule_name = document.getElementById("rule_name").value;
				var point = document.getElementById("point").value;
				var range = document.getElementById("range").value;
				var curDate = new Date();
				var endDate = calDuration(curDate,range);
				
				xmlhttp.onreadystatechange=function()
				{
					if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{    			
										document.getElementById("result").innerHTML=xmlhttp.responseText;
					}
				}
					xmlhttp.open("GET","rule_client.php?addRule="+addRule
												+"&point="+point
												+"&rule_name="+rule_name
												+"&cur_date="+curDate
												+"&end_date="+endDate,true);		
					xmlhttp.send();
				}
				
			function setRule(id){
				$("#editForm").hide("slow");
				var xmlhttp=new XMLHttpRequest();
				var setRule = true;	
				var getRules = true;				
				var rule_name = document.getElementById("erule_name").value;
				var rule_id = id;
				var point = document.getElementById("epoint").value;
				var curDate = new Date();
				xmlhttp.onreadystatechange=function()
				{
					if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{    			
										document.getElementById("result").innerHTML=xmlhttp.responseText;
					}
				}
					xmlhttp.open("GET","rule_client.php?setRule="+setRule
												+"&point="+point
												+"&rule_name="+rule_name
												+"&rule_id="+rule_id
												+"&cur_date="+curDate,true);		
					xmlhttp.send();
			}		

			function clearForm(){		
				document.getElementById("rule_name").value = "";
				document.getElementById("point").value = "";
				document.getElementById("range").value = "";
				//$("#addForm").hide("slow");
			}
			
			function clearEditForm(id){	
				$("#"+id).show();		
				document.getElementById("e"+id).innerHTML="";
			}
				
			function editRule(id,name,p){	
				$("#"+id).hide();			
				$("#editForm").show("slow");
				//$("#addForm").hide("slow");
				var table = "<td>" +id+ "</td>";
				  table += "<td><input class=\"form-control\" type=\"text\" id=\"erule_name\" value = \""+name+"\"/></td>";
				  table += "<td><input class=\"form-control\" type=\"number\" id=\"epoint\" value = \""+p+"\"/></td>";
				  table += "<td><button class=\"btn btn-success btn-sm\" type=\"button\" onclick=\"setRule("+id+")\"><span class=\"glyphicon glyphicon-floppy-disk\"></span> Save</button>";
				  table += "<button class=\"close\" type=\"button\" onclick=\"clearEditForm("+id+")\">&times;</button></td>";
				document.getElementById("e"+id).innerHTML=table;

			}
				
			function deleteRule(id){	
				if (confirm("Are you sure?")) {
				
					$("#"+id).hide("slow");	
					$("#editForm").hide("slow");
					//$("#addForm").hide("slow");	
					var xmlhttp=new XMLHttpRequest();
					var delRule = true;	
					var getRules = true;				
					var rule_id = id;
					xmlhttp.onreadystatechange=function()
					{
						if (xmlhttp.readyState==4 && xmlhttp.status==200)
						{
							document.getElementById("result").innerHTML=xmlhttp.responseText;
						}
					}
					xmlhttp.open("GET","rule_client.php?delRule="+delRule+"&rule_id="+rule_id,true);
					xmlhttp.send();
				
				}
			}

			function calDuration(curDate,range){
				var duration = new Date(curDate.getTime()+(range*24*60*60*1000));
				var d = duration.getDate();
				if(d<10) d = "0"+d;
				var m = (duration.getMonth()+1);
				if(m<10) m = "0"+m;
				var y = duration.getFullYear();
				var endDate = y+m+d;
				return endDate;
			}
			
			$(document).ready(function(){
				$("#addForm").hide();
				$("#editForm").hide();	
				
				$("#addrule").click(function(){
					/*$("#addForm").show("slow");
					document.getElementById("rule_name").value = "";
					document.getElementById("point").value = "";
					document.getElementById("range").value = "";
					$("#editForm").hide("slow");	*/
					$('#add_rule_modal').modal();
					});			
			});	
		</script>
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body onload = "showResult();">
	

<?php include "nav.php"; ?>

		<div class="container">
			<div class="row">
				
				<div class="col-sm-12 col-md-3 col-lg-3">
					<div class="box">
						<p>
							<center><img src="img/person.jpg" style="width: 80%;">
							</center>
						</p>

					</div>
				</div>
				
				<div class="col-sm-12 col-md-9 col-lg-9">
					<div class="box">
						<h1> Rules Management
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
								Edit <span class="caret"></span>
							</button>
							<ul class="dropdown-menu dropdown-normal" role="menu">
								<li>
									<a href="#">avartar</a>
								</li>
							</ul>
						</div></h1>
											
						<hr />
				
						<br/>
						
						<div class="tab-content">
							
							
							
							<div class="tab-pane active" id="show_rule" >
								<div class="panel panel-default">
									<!-- Default panel contents -->
									<div class="panel-heading">
										<h4>&nbsp;&nbsp;<span class="glyphicon glyphicon-list-alt"> </span> Rules&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-success" id="addrule" data-toggle="modal" data-target="#add_rule_modal"><span class="glyphicon glyphicon-plus"></span> Add rule</button></h4>
									</div>
		
									<!-- Table -->
										<div id="result"></div>
										
										<div class="modal fade" id="add_rule_modal">
										  <div class="modal-dialog">
											<div class="modal-content">
											  <div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												<h4 class="modal-title">Add new rule</h4>
											  </div>
											  <div class="modal-body">
												
												<div class="form-horizontal">
													<div class="form-group">
														<label for="rule_name" class="col-xs-3 col-sm-3 col-md-3 control-label">Rule Name: </label>
														<div class="col-xs-6 col-sm-6 col-md-6">
															<input type="text" class="form-control" id="rule_name" placeholder="Rule Name">
														</div>
													</div>
													<div class="form-group">
														<label for="point" class="col-xs-3 col-sm-3 col-md-3 control-label">Point(s): </label>
														<div class="col-xs-4 col-sm-4 col-md-4">
															<input type="number" class="form-control" id="point" placeholder="Point(s)">
														</div>
													</div>
													<div class="form-group">
														<label for="range" class="col-xs-3 col-sm-3 col-md-3 control-label">Valid for: </label>
														<div class="col-xs-4 col-sm-4 col-md-4">
															<input type="number" class="form-control" id="range" placeholder="Valid for">
														</div>
														<p class="col-lg-1" style="margin-left: -3.5%; margin-top: 1%;">Day(s)</p>
													</div>
												</div>
												
											  </div>
											  <div class="modal-footer">
												<button type="button" class="btn btn-default" onclick="clearForm()" data-dismiss="modal">Cancel</button>
												<button type="button" class="btn btn-primary" onclick="addRule()" data-dismiss="modal">Save</button>
											  </div>
											</div><!-- /.modal-content -->
										  </div><!-- /.modal-dialog -->
										</div><!-- /.modal -->
											<!--br>
											<div id ="addForm">
											<h2>Add Rule Form</h2>
											Rule Name: <input type="text" id="rule_name" /><br>
											Point(s): <input type="number" id="point" /><br>
											Valid for: <input type="number" id="range" /> Day(s) <br>
											<button type="button"  onclick="addRule()">Save</button>
											<button type="button"  onclick="clearForm()">Cancel</button>
											</div-->

										<div id ="editForm"></div>		

									</div>
								
							</div>
							</div>
						</div>
					</div>
					</div>

				</div>

			</div>
			
		</div>

		<div class="container box" id="footer">
			&copy; Copyright 2014 <em>Reward System</em>. All Rights Reserved.
		</div>
		<a href="#" id="scroll_top">Scroll</a>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/bootstrap.min.js"></script>
		<script src="js/scroll.js"></script>
		<script>
			$('#rule_option a:first').tab('show');
		</script>
	</body>
</html>