<?php

$adminID = 1;
require_once "lib/nusoap.php";

function createConnection($host,$username,$password,$dbname){
	// Create connection
	$con=mysqli_connect($host,$username,$password,$dbname);

	// Check connection
	if (mysqli_connect_errno()){
		return "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	return $con;
}

function closeConnection($con){
	mysqli_close($con);
}

function getRules(){
	$con = createConnection("localhost","thaifree_itapp","1DmQSoYm","thaifree_rewards");
	$query = "SELECT Rule_ID,Rule_Name,Point FROM Rules ORDER BY Rule_ID";
	
	$result = mysqli_query($con,$query);
	
	closeConnection($con);
	
	$table = displayRule($result);		// Comment th width=\"150\"is line to disable displayRule
	return $table;								// Then, change th width=\"150\"e returned value to $result
}

function addRule($ruleName,$point,$curDate,$endDate){
	$con = createConnection("localhost","thaifree_itapp","1DmQSoYm","thaifree_rewards");
	
	$query="INSERT INTO Rules (Rule_Name, Point, Duration)
				 VALUES ('".$ruleName."',".$point.",".$endDate.")";
			
	$result = mysqli_query($con,$query);
	
	if (!$result){
		die('Error: ' . mysqli_error($con));
	}
	
	$query = "SELECT max(Rule_ID) as Rule_ID FROM `Rules`";
	$result = mysqli_query($con,$query);
	$ruleIDArray = mysqli_fetch_array($result);
	$ruleID = $ruleIDArray['Rule_ID'];
	
	$query="INSERT INTO RulesLog (Rule_ID,Admin_ID,transactionTimestamp,Action)
			VALUES (".$ruleID.",".$GLOBALS['adminID'].","."now()".",".'"add"'.")";
	$result = mysqli_query($con,$query);
	if (!$result){
		die('Error: ' . mysqli_error($con));
	}
	
	closeConnection($con);
	$table = getRules();
	return $table;
	//return "Add Successful";
}

function setRule($ruleID,$ruleName,$point,$curDate){
	$con = createConnection("localhost","thaifree_itapp","1DmQSoYm","thaifree_rewards");
	
	$query="UPDATE Rules
				 SET Rule_Name='".$ruleName."',Point=".$point." ".
				 "WHERE Rule_ID=".$ruleID;
			
	$result = mysqli_query($con,$query);

	if (!$result){
		die('Error: ' . mysqli_error($con));
	}
	$query="INSERT INTO RulesLog (Rule_ID,Admin_ID,transactionTimestamp,Action)
			VALUES (".$ruleID.",".$GLOBALS['adminID'].","."now()".",".'"add"'.")";
	$result = mysqli_query($con,$query);
	if (!$result){
		die('Error: ' . mysqli_error($con));
	}
	
	closeConnection($con);
	$table = getRules();
	return $table;
	//return "Update Successful";
}

function deleteRule($ruleID){	
	$con = createConnection("localhost","thaifree_itapp","1DmQSoYm","thaifree_rewards");
	
	$query="DELETE FROM Rules WHERE Rule_ID=".$ruleID;
			
	$result = mysqli_query($con,$query);
	
	if (!$result){
		die('Error: ' . mysqli_error($con));
	}
	
	$query="INSERT INTO RulesLog (Rule_ID,Admin_ID,transactionTimestamp,Action)
			VALUES (".$ruleID.",".$GLOBALS['adminID'].","."now()".",".'"delete"'.")";
	$result = mysqli_query($con,$query);
	if (!$result){
		die('Error: ' . mysqli_error($con));
	}

	
	closeConnection($con);
	$table = getRules();
	return $table;
	//return "Delete Successful";	
}

function displayRule($result){
	$table = "<table class=\"table table-hover\">
		<thead>
			<tr>
				<th width=\"150\">Rule_ID</th>
				<th width=\"150\">Rule_Name</th>
				<th width=\"150\">Point</th>
				<th width=\"120\">Option</th>
			</tr>
		</thead>
		<tbody>";

	while($row = mysqli_fetch_array($result))
	  {
		  $table .= "<tr id=\"" . $row['Rule_ID'] . "\">";
		  $table .= "<td>" . $row['Rule_ID'] . "</td>";
		  $table .= "<td>" . $row['Rule_Name'] . "</td>";
		  $table .= "<td>" . $row['Point'] . "</td>";
		  $table .= "<td>
						<button type=\"button\" class=\"btn btn-warning btn-sm\" id=\"edit\" onclick=\"editRule(" . $row['Rule_ID'] .",'". $row['Rule_Name'] ."','". $row['Point'] ."')\"><span class=\"glyphicon glyphicon-pencil\"></span> Edit</button>
						<button type=\"button\" class=\"btn btn-danger btn-sm\" onclick=\"deleteRule(" . $row['Rule_ID'] . ")\"><span class=\"glyphicon glyphicon-remove\"></span> Delete</button>		  
					</td>";
		  $table .= "</tr><tr id = \"e" . $row['Rule_ID'] . "\"></tr>";
	  }
	$table .= "</tbody>";
	$table .= "</table>";
	
	return $table;
}

// create object to deal with width=\"150\" service provider
$server = new soap_server();
$server->register("getRules");
$server->register("addRule");
$server->register("setRule");
$server->register("deleteRule");

if(!isset($HTTP_RAW_POST_DATA))
	$HTTP_RAW_POST_DATA = file_get_contents('php://input');

$server->service($HTTP_RAW_POST_DATA);

?>